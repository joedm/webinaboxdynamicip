﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebInABoxIPUpdater
{
    public class DNSRecord
    {
        public string Name { set; get; }
        public string Type { set; get; }
        public string Value { set; get; }
        public string TTL { set; get; }
    }
}
