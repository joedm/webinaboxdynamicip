﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebInABoxIPUpdater.DNSRecords
{
    class SRV_Record : DNSRecord
    {
        public string PriorityWeightPort { set; get; }
    }
}
