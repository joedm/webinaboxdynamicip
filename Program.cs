﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebInABoxIPUpdater
{
    class Program
    {
        static PublicIP publicIP;

        static void Main(string[] args)
        {
            publicIP = new PublicIP();
            Console.WriteLine(publicIP.IP);
            testpost();
        }

        private static void testpost()
        {
            WebClient client = new WebClient();

            var page = client.DownloadString("https://members.webinabox.net.au/security/login");

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);

            var data = new NameValueCollection();
            //trying to scrape authentication token from hidden input value. Not having much luck.
            data["authenticity_token"] = doc.DocumentNode.SelectSingleNode("/html/body/div/div/div[4]/div/div/div/div[4]/input").Attributes["value"].Value;
            data["email"] = "caleb@liquidfusion.com.au";
            data["password"] = "!Passw0rd";
            data["commit"] = "Log In";

            var response = client.UploadValues("https://members.webinabox.net.au/security/login", "POST", data);

            //tried to load a page only available when logged in
            page = client.DownloadString("https://members.webinabox.net.au/accounts/2309/services/7137/manage-DNS");
            doc.LoadHtml(page);

            try
            {
                //try to scrape data from the page
                Console.WriteLine(doc.DocumentNode.SelectSingleNode("/html/body/div/div/div[4]/div/div/div/table/tbody/tr[2]/td[2]").InnerText.ToString());
            }
            catch { }
            try
            {
                //try to scrape data from a page login page to see if it redirected there.
                Console.WriteLine(doc.DocumentNode.SelectSingleNode("/html/body/div/div/div[4]/div/div/div/span/a").InnerText.ToString());
            }
            catch { }
        }
    }
}
