﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebInABoxIPUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            PublicIP publicIP1 = new PublicIP();
            PublicIP publicIP2 = new PublicIP();
            PublicIP publicIP3 = new PublicIP();
            PublicIP publicIP4 = new PublicIP();
            PublicIP publicIP5 = new PublicIP();
            publicIP1.fetchIP();
            publicIP2.fetchIP();
            publicIP3.fetchIP();
            publicIP4.fetchIP();
            publicIP5.fetchIP();

            Console.WriteLine(publicIP1.IP);
            Console.WriteLine(publicIP2.IP);
            Console.WriteLine(publicIP3.IP);
            Console.WriteLine(publicIP4.IP);
            Console.WriteLine(publicIP5.IP);
            
        }


    }
}
