﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace WebInABoxIPUpdater
{
    class PublicIP
    {
        public string IP { set; get; }

        public void fetchIP()
        {
            WebClient client = new WebClient();

            var page = client.DownloadString("http://www.whatsmyip.org");

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);

            IP = doc.DocumentNode.SelectSingleNode("/html/body/header/div/h1").InnerText.ToString();


        }
    }
}
